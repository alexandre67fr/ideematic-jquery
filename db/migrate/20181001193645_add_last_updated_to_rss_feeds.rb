class AddLastUpdatedToRssFeeds < ActiveRecord::Migration[5.2]
  def change
    add_column :rss_feeds, :last_updated, :timestamp
  end
end
