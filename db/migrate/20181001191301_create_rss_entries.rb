class CreateRssEntries < ActiveRecord::Migration[5.2]
  def change
    create_table :rss_entries do |t|
      t.references :rss_feed, foreign_key: true
      t.string :title
      t.text :content
      t.date :date
      t.boolean :is_read

      t.timestamps
    end
  end
end
