require "application_system_test_case"

class RssEntriesTest < ApplicationSystemTestCase
  setup do
    @rss_entry = rss_entries(:one)
  end

  test "visiting the index" do
    visit rss_entries_url
    assert_selector "h1", text: "Rss Entries"
  end

  test "creating a Rss entry" do
    visit rss_entries_url
    click_on "New Rss Entry"

    click_on "Create Rss entry"

    assert_text "Rss entry was successfully created"
    click_on "Back"
  end

  test "updating a Rss entry" do
    visit rss_entries_url
    click_on "Edit", match: :first

    click_on "Update Rss entry"

    assert_text "Rss entry was successfully updated"
    click_on "Back"
  end

  test "destroying a Rss entry" do
    visit rss_entries_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Rss entry was successfully destroyed"
  end
end
