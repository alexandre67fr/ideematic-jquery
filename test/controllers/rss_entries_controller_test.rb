require 'test_helper'

class RssEntriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @rss_entry = rss_entries(:one)
  end

  test "should get index" do
    get rss_entries_url
    assert_response :success
  end

  test "should get new" do
    get new_rss_entry_url
    assert_response :success
  end

  test "should create rss_entry" do
    assert_difference('RssEntry.count') do
      post rss_entries_url, params: { rss_entry: {  } }
    end

    assert_redirected_to rss_entry_url(RssEntry.last)
  end

  test "should show rss_entry" do
    get rss_entry_url(@rss_entry)
    assert_response :success
  end

  test "should get edit" do
    get edit_rss_entry_url(@rss_entry)
    assert_response :success
  end

  test "should update rss_entry" do
    patch rss_entry_url(@rss_entry), params: { rss_entry: {  } }
    assert_redirected_to rss_entry_url(@rss_entry)
  end

  test "should destroy rss_entry" do
    assert_difference('RssEntry.count', -1) do
      delete rss_entry_url(@rss_entry)
    end

    assert_redirected_to rss_entries_url
  end
end
