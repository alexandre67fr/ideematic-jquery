#= require jquery
#= require bootstrap

jQuery(document).ready ($) ->

	# Load all feeds
	load_feeds = ->
		$.ajax
			url: "/rss_feeds.html"
			success: (html) ->
				$('.rss-feeds').html html

	# Mark entry as read link
	$(document).on 'click', '.rss-entry .mark-as-read', ->
		entry = $(this).closest '.rss-entry'
		entry.addClass 'is-read'
		$.ajax
			type: "PATCH"
			url: "/rss_entries/#{entry.data 'id'}.json"
			data: 
				rss_entry:
					is_read: 1
		false

	# Mark entry as as read, when clicking headline
	$(document).on 'click', '.rss-entry h5 a', ->
		entry = $(this).closest '.rss-entry'
		entry.find('.mark-as-read:visible').click()
		true

	# Delete a feed
	$(document).on 'click', '.rss-feed .close', ->
		feed = $(this).closest '.rss-feed'
		$.ajax
			type: "DELETE"
			url: "/rss_feeds/#{feed.data 'id'}.json"
		feed.remove()
		false

	# Click on feed URL examples
	$(document).on 'click', '.feed-url-examples a', ->
		link = $ this
		form = link.closest('form')
		form.find('[name="rss_feed[url]"]').val link.text()
		false

	# Pagination
	$(document).on 'click', '.rss-feed .pagination a', ->
		link = $ this
		link.parent().addClass 'disabled'
		feed = link.closest '.rss-feed'
		top = feed.offset().top - 10
		$('html, body').animate
			scrollTop: top
		,
			duration: 500
		$.ajax
			url: link.attr('href')
			success: (html) ->
				feed.replaceWith html
		false

	# Add a feed
	$(document).on 'submit', '#add-feed form', ->
		form = $ this
		form.find('[type="submit"]').prop 'disabled', true
		$.ajax
			type: "POST"
			url: "/rss_feeds.json"
			data: form.serialize()
			success: ->
				$('.modal').modal 'hide'
				form.find('[type="submit"]').prop 'disabled', false
				form.find('input').val ''
				load_feeds()
		false
