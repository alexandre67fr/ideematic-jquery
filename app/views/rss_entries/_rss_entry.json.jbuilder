json.extract! rss_entry, :id, :created_at, :updated_at
json.url rss_entry_url(rss_entry, format: :json)
