module RssFeedsHelper

	def self.update_feeds
		RssFeed.all.each do |feed|
			self.update_feed feed
		end
	end

	def self.update_feed feed
		require 'rss'
	  	require 'open-uri'
	  	begin
	  		fetched_rss_content = open(feed.url, read_timeout: 5).read
	  		fetched_rss_entries = RSS::Parser.parse(fetched_rss_content, false).items
			# feed.rss_entries.destroy
	  		ActiveRecord::Base.transaction do
				fetched_rss_entries.each do |fetched_rss_entry|
					return if feed.rss_entries.find_by_url(fetched_rss_entry.link)
					rss_entry = RssEntry.new
					rss_entry.rss_feed = feed
					rss_entry.title = fetched_rss_entry.title
					rss_entry.date = fetched_rss_entry.pubDate || fetched_rss_entry.dc_date
					rss_entry.content = fetched_rss_entry.description
					rss_entry.url = fetched_rss_entry.link
					rss_entry.save! 
				end
			end
		rescue
			# TODO: tell user if something is wrong with the URL
		end
	end


end
